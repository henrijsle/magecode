<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function signup() {
        return view('user.signup');
    }

    public function save(Request $request) {
        // Formas validācija
        $validated = $request->validate([
            'name' => ['required', 'max:255'],
            'email' => ['required', 'unique:user', 'max:255'],
        ]);

        // Jauna lietotāja izveide
        $user = New User();
        $user->name = request('name');
        $user->email = request('email');
        $user->save();
    
        return redirect('/signup')->with('success', 'Profils veiksmīgi izveidots!');
    }
}
