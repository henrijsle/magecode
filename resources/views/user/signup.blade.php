@extends('layout.main')

@section('content')
    <div class="container">
        <!-- Nepareizi aizpildītas formas alerti -->
        @if ($errors->any())
            <div class="danger">
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        @endif

        <!-- Veiksmīgi aizpildītas un saglabātas formas alerts -->
        @if (session('success'))
            <div class="success">
                {{ session('success') }}
            </div>
        @endif

        <!-- Forma ar name un email laukiem -->
        <form method="POST" action="{{ route('user.save') }}">
            @csrf
            <div class="form">
                <label for="name">Name</label>
                <input type="text" id="name" name="name" placeholder="Name">
            </div>
            <div class="form">
                <label for="email">E-mail</label>
                <input type="email" id="email" name="email" placeholder="E-mail">
            </div>
            <button class="submit" type="submit">Submit</button>
        </form>
    </div>
@endsection