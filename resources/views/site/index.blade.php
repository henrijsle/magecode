@extends('layout.main')

@section('content')

    <div class="index-grid">
        <!-- Kreisā puse -->
        <div class="index-content">
            <!-- Virsraksti -->
            <header class="index-header">
                <h1 class="index-h1">Digital</h1>
                <h3 class="index-h3">Marketing</h3>
            </header>
            <!-- Teksts -->
            <p class="index-p">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
            <!-- Learn more poga -->
            <div class="index-learmore">
                <a href="{{ route('site.about') }}">Learn More</a>
            </div>
        </div>
        <!-- Labā puse -->
        <div class="index-img">
            <img src="{{asset('img/illustration.jpg')}}">
        </div>
    </div>

@endsection