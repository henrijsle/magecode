<header class="header-grid">
    <!-- Logo -->
    <div class="header-logo">
        <a href="{{ route('site.index') }}" >Logo</a>
    </div>
    <!-- Telefona versijas hamburger'a ikona -->
    <div class="hamburger-icon">
        <a href="javascript:void(0);" class="hamburger-icon" onclick="myFunction()">
            <i class="fa fa-bars"></i>
        </a>
    </div>
    <!-- Navigācijas izvēlne -->
    <nav class="header-nav">
        <ul id="header-ul">
            <li><a href="{{ route('site.index') }}">Home</a></li>
            <li><a href="{{ route('site.service') }}">Services</a></li>
            <li><a href="{{ route('site.about') }}">About</a></li>
            <li><a href="{{ route('site.contact') }}">Contact</a></li>
            <li><a href="{{ route('site.faq') }}">FAQ</a></li>
            <li><a href="{{ route('user.signup') }}" class="nav-signup">Sign Up</a></li>
        </ul>
    </nav>
</header>