<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Magecode</title>
</head>
<body class="grid">
    <!-- Header/Navigation -->
    @include('layout.header')
    <!-- Main content -->
    <main class="main-grid">
        @yield('content')
    </main>
</body>
<script>
    // Telefona versijas hamburger izvēlnes show/hide funkcija
    function myFunction() {
        var x = document.getElementById("header-ul");
        if (x.style.display === "block") {
            x.style.display = "none";
        } else {
            x.style.display = "block";
        }
    }
</script>
</html>