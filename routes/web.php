<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Site Routes
Route::get('/', [SiteController::class, 'index'])->name('site.index');
Route::get('/service', [SiteController::class, 'service'])->name('site.service');
Route::get('/about', [SiteController::class, 'about'])->name('site.about');
Route::get('/contact', [SiteController::class, 'contact'])->name('site.contact');
Route::get('/faq', [SiteController::class, 'faq'])->name('site.faq');

// User Routes
Route::get('/signup', [UserController::class, 'signup'])->name('user.signup');
Route::post('/create', [UserController::class, 'save'])->name('user.save');
